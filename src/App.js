
import './App.css';
//dependencies
import {Route, Routes} from 'react-router-dom';
import {BrowserRouter as Router} from 'react-router-dom';
import {Container} from 'react-bootstrap';

import React, {useState, useContext, useEffect} from 'react';
import { UserProvider } from './UserContext';

// pages
import Home from './pages/Home';
import AdminDashboard from './pages/AdminDashboard';
import ProductView from './pages/ProductView';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products'
import Error from './pages/Error';
import ProductOrders from './pages/ProductOrders';
import SearchProduct from './pages/SearchProduct';
import Profile from './pages/Profile';
import UserRole from './pages/UserRole';

// components
import AppNavbar from './components/AppNavbar';


export default function App() {

  // 1 - create state
  const[user, setUser] = useState({
    'id': localStorage.getItem('id')
  })
  console.log(user);

  const unsetUser = () => {
    localStorage.clear();
  }

// corrected user credentials
  // const unsetUser = () =>{
  //   setUser({
  //     id: null,
  //     isAdmin: null
  //   })
  // }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => result.json())
    .then(data => {
      if(typeof data._id !== undefined){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
      console.log(data);


    })
  },[])


  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
    <Router>
        <AppNavbar />
        <Container fluid>
          <Routes>
            <Route exact path="/" element={<Home />}/>
            <Route exact path="/dashboard" element={<AdminDashboard />} />
            <Route exact path="/dashboard/products/orders" element={<ProductOrders />} />
            <Route exact path="/dashboard/users/role" element={<UserRole />} />
            <Route exact path="/profile" element={<Profile />}/>
            <Route exact path="/products/find" element={<SearchProduct />} />
            <Route exact path="/register" element={<Register />}/>
            <Route exact path="/login" element={<Login />}/>
            <Route exact path="/logout" element={<Logout />}/>
            <Route exact path="/products" element={<Products />}/>
            <Route exact path="/products/:productId" element={<ProductView />} />
            <Route exact path="*" element={<Error />}/>
          </Routes>
        </Container>  
    </Router>
    </UserProvider>
  );
}

