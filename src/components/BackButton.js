import Icon from '@mdi/react';
import { mdiArrowLeft, mdiArrowRight } from '@mdi/js';
import { useNavigate } from 'react-router-dom';


export default function BackButton (){
	let navigate = useNavigate();
	return(
		<div style={{position: 'absolute', left: 50 }}>
			<button onClick={() => navigate(-1)}><Icon path={mdiArrowLeft} size={1}/></button>
			{/*<button onClick={() => navigate(1)}><Icon path={mdiArrowRight} size={1}/></button>*/}
		</div>
		
	)
}

