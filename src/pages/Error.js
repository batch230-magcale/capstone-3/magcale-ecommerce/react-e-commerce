import {useContext} from 'react';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

import {BrowserRouter as Router, useLocation} from 'react-router-dom';

export default function Error(){
	const {user} = useContext(UserContext);


	return(
		
		<>
		{(user.isAdmin)?
		<>
		<h1>Page Not Found</h1>
		<p>Go back to the <Link as={Link} to="/dashboard">Dashboard</Link>.</p>
		</>
		:
		<>
			<h1>Page Not Found</h1>
			<p>Go back to the <Link as={Link} to="/">Homepage</Link>.</p>
		</>
		}
		</>
		
		
	)
}