import AppNavbar from '../components/AppNavbar';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Card, Button, Table, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './Home.css';

export default function Profile (){
	const {user, setUser} = useContext(UserContext);
	console.log(user)
	const [userId, setUserId] = useState('');

	const [firstName, setfirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	
	const [totalAmount, setTotalAmount] = useState(0);
	const [purchasedOn, setPurchasedOn] = useState('');
	const [products, setProducts] = useState([]);

	const [orders, setOrders] = useState([]);


	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				'Content-Type': 'applications/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);
			setfirstName(data.firstName);
			setLastName(data.lastName);
			setEmail(data.email);
			setPassword(data.password);
			setMobileNumber(data.mobileNumber);
			console.log(data)

			setOrders(
				fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/getUserOrders`)
			)

		})

	}, [])
	console.log(mobileNumber)
	console.log(totalAmount);
	return(
		<Card className="col-md-5 mt-5 pt-5 mx-auto text-center">
		<div id="profile-bg"  id="bg-card">
			<h2 className="mb-5 mx-auto">{firstName}'s Profile</h2>
			<Row>
				<Col>
					<Card.Text className="pb-4">First name:</Card.Text>
					<Card.Text className="pb-4">Last name:</Card.Text>
					<Card.Text className="pb-4">Email:</Card.Text>
					<Card.Text className="pb-4">Password:</Card.Text>
					<Card.Text className="pb-4">Mobile number:</Card.Text>
				</Col>
				<Col>
					<Card.Subtitle className="pb-5">{firstName}</Card.Subtitle>
					<Card.Subtitle className="pb-5">{lastName}</Card.Subtitle>
					<Card.Subtitle className="pb-5">{email}</Card.Subtitle>
					<Card.Subtitle className="pb-5">{password}</Card.Subtitle>
					<Card.Subtitle className="pb-5">{mobileNumber}</Card.Subtitle>
				</Col>
				
				
				
				
			</Row>
		</div>
		</Card>
		
	)
}