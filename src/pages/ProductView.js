import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';

import {BrowserRouter as Router, useLocation} from 'react-router-dom';

import BackButton from '../components/BackButton';

export default function ProductView(){
	const {user} = useContext(UserContext);
	console.log(user);

	const navigate = useNavigate();

	const {productId} = useParams();
	

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const [stocks, setStocks] = useState();
	const [isActive, setIsActive] = useState();
	const total = quantity*price;

	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(result => result.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks);
			setIsActive(data.isActive)
		})

	}, [productId])

	const provideQuantity = (event) => {
	  const buttonValue = event.target.value;
	  if (buttonValue == 1) {
	    setQuantity(prevQuantity => prevQuantity + 1);
	  }
	  if (buttonValue == -1) {
	    setQuantity(prevQuantity => prevQuantity - 1);
	  }

	}

	const checkout = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId : productId,
				quantity: quantity
			})

		})
		.then(result => result.json())
		.then(data => {
			console.log('Checkout data');
			console.log(data);

			if(data){
				Swal.fire({
					title: 'Checkout successful',
					icon: 'success',
					text: 'You have successfully checkedout this product.'
				})

				navigate('/dashboard/products/orders')
			}
			if(quantity == 0){
				Swal.fire({
					title: 'Invalid input',
					icon: 'error',
					text: `Please specify quantity.`,
				})
			}

		})
		
	}

	

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span: 6, offset: 3}}>
					<Card>
						<Card.Body className="text-center">
							<h1 className="mb-5">{name}</h1>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php. {price}</Card.Text>
							<BackButton />
							<Card.Subtitle>Stocks:</Card.Subtitle>
							<Card.Text>{stocks}</Card.Text>
							<Card.Subtitle>Quantity:</Card.Subtitle>
							<Card.Text>
								<Button variant="bg-muted" value={-1} onClick={provideQuantity} disabled={quantity == 0}>-</Button>
									{quantity}
								<Button variant="bg-muted" value={1} onClick={provideQuantity} disabled={quantity == stocks}>+</Button>
							</Card.Text>
							<Card.Subtitle>Total:</Card.Subtitle>
							<Card.Text>{total}</Card.Text>
							{
								(user.id !== null && user.isAdmin == false && isActive == true)
								?
									
									<Button id="product-view-btn" variant="primary"  size="lg" onClick={() => checkout(productId)}>Checkout</Button>
								:
									<Button as={Link} to="/login" variant="secondary" size="lg" disabled>Out of stock</Button>
							}

						</Card.Body>	
					</Card>
				</Col>
			</Row>
		</Container>
	)


}