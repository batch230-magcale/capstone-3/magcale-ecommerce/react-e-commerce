import { useContext, useState, useEffect} from 'react';
import UserContext from '../UserContext';
import {Table, Container, Card, Row, Col, Button} from 'react-bootstrap';

import {Link} from 'react-router-dom';
import { useNavigate } from 'react-router-dom';

import Icon from '@mdi/react';
import {mdiAccountMultipleOutline, mdiArrowLeft, mdiArrowRight } from '@mdi/js';

export default function ProductOrders (){
	let navigate = useNavigate();

	const {user} = useContext(UserContext);
	console.log(user);

	const [orderDetails, setOrderDetails] = useState([]);

	// const [name, setName] = useState('');
	// const [description, setDescription] = useState('');
	// const [price, setPrice] = useState(0);
	// const [stocks, setStocks] = useState(0);
	const [isActive, setIsActive] = useState(Boolean);
	// const [orders, setOrders] = useState([]);
	// const [userId, setUserId] = useState('');
	// const [userEmail, setUserEmail] = useState('');
	// const [quantity, setQuantity] = useState('');
	// const [purchasedOn, setPurchasedOn] = useState('');

	// for show orders
	const [totalAmount, setTotalAmount] = useState(0);
	const [products, setProducts] = useState([]);
	const [productId, setProductId] = useState('');
	const [productName, setProductName] = useState('');
	const [userOrders, setUserOrders] = useState([])



	const productDetails = () =>{fetch(`${process.env.REACT_APP_API_URL}/products/productWithOrders`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);
	
			setOrderDetails(data.map(product => {
				const orderList = product.orders.map(order => (
						<tr key={order._id}>
							<td>{product._id}</td>
							<td>{product.name}</td>
							{/*<td>{product.description}</td>*/}
							<td>{product.price}</td>
							<td>{product.stocks}</td>
							{/*<td>{product.isActive? 'Active' : 'Inactive'}</td>*/}
							<td>{order.userId}</td>
							<td>{order.userEmail}</td>
							<td>{order.quantity}</td>
							<td>{order.quantity * product.price}</td>
							<td>{order.purchasedOn}</td>
						</tr>
				))
				return orderList;
			}))
		}
	)}




	const showUserOrders = () =>{

		fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/getUserOrders`,{
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			console.log(data)

			setUserOrders(data.map(checkout => {
				const viewOrders = checkout.products.map(userOrder => (
						<Card key={userOrder._id} className="col-md-5 m-5 p-3 mx-auto" variant="secondary">
							<Container>
								<Row>
								<Col>
								<Card.Subtitle className="text-secondary">Product name:</Card.Subtitle>
									<Card.Text>{userOrder.productName}</Card.Text>

								<Card.Subtitle className="text-secondary">Quantity:</Card.Subtitle>
									<Card.Text>{userOrder.quantity}</Card.Text>

								<Card.Subtitle className="text-secondary">Total Amount:</Card.Subtitle>
									<Card.Text>{checkout.totalAmount}</Card.Text>
								</Col>
								<Col className="pt-2 mt-2">
								<Card.Subtitle className="text-secondary">Product ID:</Card.Subtitle>
									<Card.Text>{userOrder.productId}</Card.Text>

								<Card.Subtitle className="text-secondary">Transaction date:</Card.Subtitle>
									<Card.Text className="mb-1 pb-1">{checkout.purchasedOn}</Card.Text>
								</Col>
								<Col className="m-md-3 p-md-3">
								{(userOrder.isActive === true)
									?
									<Button id="order-btn" as={Link} to={`/products/${userOrder.productId}`}>Order again</Button>
									:
									<Button id="order-btn" variant="secondary" disabled>Out of stock</Button>
								}
								</Col>
								
								</Row>
							</Container>
						</Card>
				))
				return viewOrders;
			}))
		})
	}

	console.log(isActive);

	useEffect(()=>{
		if(user.isAdmin == true){
			productDetails();
		}
		if(user.isAdmin == false){
			showUserOrders();
		}
	}, [])

	if(user.isAdmin == true){
		return(
			<Container className="table-responsive">
				<div className="mt-5 mb-3 text-center">
					<h1>Product Checkouts</h1>
					<Button variant="primary" className="mx-2" onClick={() => navigate(-1)}><Icon path={mdiArrowLeft} size={1}/></Button>
					
				</div>
				<Table bordered>
					<thead>
						<tr>
							<th className="text-center">Product ID</th>
							<th className="text-center">Product Name</th>
							{/*<th>Description</th>*/}
							<th className="text-center">Price</th>
							<th className="text-center">Current Stocks</th>
							{/*<th>Status</th>*/}
							<th className="text-center">User ID</th>
							<th className="text-center">User email</th>
							<th className="text-center">Quantity (pcs.)</th>
							<th className="text-center">Total Amount (Php)</th>
							<th className="text-center">Transaction date</th>
						</tr>
					</thead>
					<tbody className="text-center">
						{orderDetails}
					</tbody>
				</Table>
			</Container>
		)
	}
	if(user.isAdmin == false){
		return(
			<>
			<h1 className="text-center">Order History</h1>
			
			{userOrders}
			
			</>
		)
	}
}