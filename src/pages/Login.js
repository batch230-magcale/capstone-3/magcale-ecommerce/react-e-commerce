import {Form, Button, Stack} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';

import Swal from 'sweetalert2';

import {BrowserRouter as Router, useLocation} from 'react-router-dom';

export default function Login(){

	// 3 - 
			// variable, setter function
	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);
	const [firstName, setfirstName] = useState('');

	function authenticate(event){
		
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				// 'Authorization': `Bearer ${localStorage.getItem('access')}`
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);
			console.log('Check accessToken');
			console.log(data.access);
			

			if(typeof data.access !== 'undefined'){
				localStorage.setItem('token', data.access);
				console.log(typeof data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: `Welcome!`
				})
			}
			else{
				Swal.fire({
					title: 'Authentication failed',
					icon: 'error',
					text: 'Check your login details and try again.'
				})
			}


		})

		setEmail('');
	}

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password])

	// redirecting to admin dashboard
	if	(user.isAdmin == true && localStorage.getItem('token')){	
		return <Navigate to="/dashboard"/>}
	
	// redirecting to user dashboard (products)
	if	(user.isAdmin == false && localStorage.getItem('token')){
		return <Navigate to="/"/>}
		
	else{
		return(
		<Stack className="col-md-5 mt-5 pt-5 ms-auto">
				<Form className="m-5" onSubmit={event => authenticate(event)}>
					<h1>Login</h1>
		            <Form.Group controlId="userEmail">
		                <Form.Label>Email address</Form.Label>
		                <Form.Control 
			                type="email" 
			                placeholder="Enter email" 
			                value = {email}
			                onChange = {event => setEmail(event.target.value)}
			                required
		                />
		            </Form.Group><br/>

		            <Form.Group controlId="password1">
		                <Form.Label>Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Password"
			                value = {password}
			                onChange= {event => setPassword(event.target.value)} 
			                required
		                /><br/>
		            </Form.Group>
		            {isActive ? //true
			            <Button variant="primary" type="submit" id="login-btn" onClick={event => authenticate(event)}>
			            	Login
			            </Button>
		            	: //false
		            	<Button variant="primary" type="submit" id="login-btn" disabled>
		            		Login
		            	</Button>
		        	}
		        </Form>
	        </Stack>
		)
	}



}