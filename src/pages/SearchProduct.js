// // import coursesData from '../data/coursesData';
	// import { Navigate, useParams, Link } from 'react-router-dom';
	// // import ProductCard from '../components/ProductCard';
	// import { Fragment, useEffect, useState, useContext } from 'react';

	// import UserContext from '../UserContext';
	// import ProductList from '../components/ProductList';
	// import SearchBar from '../components/SearchBar';

	// import {Button, Form, InputGroup, Col,Container, Card} from 'react-bootstrap'
	// import Icon from '@mdi/react';
	// import { mdiMagnify } from '@mdi/js';
	// import { PropTypes } from 'prop-types';



	// export default function SearchProduct({searchProp}){

	//   // const { name, description, price, stocks } = searchProp;
	//   // console.log(searchProp)

	//   return (
	//     <div>
	// {/*      <p>Product ID: {_id}</p>
	//       <p>Name: {name}</p>
	//       <p>Description: {description}</p>
	//       <p>Price: {price}</p>
	//       <p>Stocks: {stocks}</p>*/}
	//     </div>
	//   );
	// }

	// SearchProduct.propTypes = {
	// 	searchProp: PropTypes.shape({
	// 		name: PropTypes.string.isRequired,
	// 		description: PropTypes.string.isRequired,
	// 		stocks: PropTypes.number.isRequired,
	// 		price: PropTypes.number.isRequired
	// 	})
	// }

import {Form, Button, InputGroup, Container, Card, Row, Col} from 'react-bootstrap';
import {Icon} from '@mdi/react';
import {mdiMagnify} from '@mdi/js';
import { Link } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
// import SearchProduct from '../pages/SearchProduct';
import {BrowserRouter as Router, useLocation} from 'react-router-dom';
import UserContext from '../UserContext';

export default function SearchProduct (){
	const { user } = useContext(UserContext);

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState();
	const [products, setProducts] = useState();
	console.log(products);

	const findItems = () =>{

		fetch(`${process.env.REACT_APP_API_URL}/products/search`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('access')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				// price: price,
				// stocks: stocks
			})
		})
		.then(result => result.json())
		.then(data => {
			console.log('fetching result');
			console.log(data);	
			setProducts(data.map(product => {
				console.log(product)
				return (
				    
					<Card className="col-md-10 m-3 mx-auto">
					    <Card.Body>
					    	<Row>
					    		<Col>
					    			<Card.Title className="text-center">{name}</Card.Title>
					    		</Col>
					    		{(user.isAdmin)
					    		?
					    		<></>
					    		:
					    		<>
					    			{(user.id == null)?
					    			<Button id="product-find-btn" className="mb-2 mt-2" as={Link} to={`/login`}>Login first</Button>
					    			:
						    		<Button id="product-find-btn" className="mb-2 mt-2" as={Link} to={`/products/${product._id}`}>Checkout</Button>
						    		}
					    		</>
					    		}
					    	</Row>
					    	<Row>
					        {/*<Card.Title>{name}</Card.Title>*/}
					        	<Col md={{offset: 2}}>
					        		<h6>Description:</h6>
					        		<Card.Text>{product.name}</Card.Text>
					        	</Col>
					        	<Col>
					        		<h6>Price:</h6>
					        		<Card.Text>{product.price}</Card.Text>
					        	</Col>
					        	<Col>
					        		<h6>Stocks:</h6>
					        		<Card.Text>{product.stocks}</Card.Text>
					        	</Col>
					        {/*	<Col className="text-center">
					        		<Button as={Link} to={`/products/${product._id}`}>View</Button>
					        	</Col>*/}
					        </Row>
					    
					    </Card.Body>
					</Card>
				)
				
			}))		
		})
	}

	useEffect(()=>{
		findItems();
	}, [])

	console.log(products);

	if(products && products.length > 0){
		return(
			<Container>
			<InputGroup className="col-md mb-3 mt-5" >
			  	<Form.Control 
			  		placeholder="Search product" 
			  		value={name}
			  		onChange={e => setName(e.target.value)}
			  			/>
			  	<Button className="" type="submit" onClick={findItems} as={Link} to="/products/find" ><Icon path={mdiMagnify} size={1}/></Button>
			</InputGroup>
				{products}
			</Container>
		)
	}
	else{
		return(
			<Container>
			<InputGroup className="col-md mb-3 mt-3"  >
			  	<Form.Control 
			  		placeholder="Search product" 
			  		value={name}
			  		onChange={e => setName(e.target.value)}
			  			/>
			  	<Button type="submit" as={Link} to="/products/find" onClick={e => findItems(e)}><Icon path={mdiMagnify} size={1}/></Button>
			</InputGroup>
			</Container>
		)
	}
}