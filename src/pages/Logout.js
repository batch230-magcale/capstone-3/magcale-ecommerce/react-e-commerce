import {Navigate} from 'react-router-dom';
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext';

import {BrowserRouter as Router, useLocation} from 'react-router-dom';

export default function Logout(){

	const {unsetUser, setUser} = useContext(UserContext);
	
	unsetUser();

	useEffect(() =>{
		setUser({id: null})
	})

	return(
		<Navigate to="/login" />
	)

/*	if(user.token !== null){
		unsetUser();
		return <Navigate to="/products/active" /> // lalagyan ng ko pa ng error component
	}
	
	if(user.token == null){
	

	// useEffect(() =>{
	// 	setUser({id: null})
	// })

	return(
		<Navigate to="/login" />
	)
	}*/

}