import {Link, BrowserRouter as Router, useLocation} from 'react-router-dom';
import {Container, Row, Col, Button} from 'react-bootstrap';
import './Home.css';
import UserContext from '../UserContext';
import {useContext} from 'react';
import Products from './Products';

export default function Home(){
	const {user} = useContext(UserContext);
	console.log(user);

	if(user.isAdmin == false){
		return(
			<Container id="home-container" className="pt-5 mt-5">	
				<Col md={{offset: 8}}>	
				<Button id="home-btn" as={Link} to="/products" style={{fontSize: '60px', marginTop: '150px'}}>Shop now</Button>
				</Col>
			</Container>
		)
	}
	if(user.id == null){
		return(
			<Container id="home-container" className="pt-5 mt-5">	
				<Col md={{offset: 8}}>	
				<Button id="home-btn" as={Link} to="/login" style={{fontSize: '60px', marginTop: '150px'}}>Shop now</Button>
				</Col>
			</Container>
		)
	}
}